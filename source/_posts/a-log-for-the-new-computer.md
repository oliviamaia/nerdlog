---
title: a log for the new computer
date: 2021-02-03 19:28:12
---

Yes, that is one week of the new computer, more or less. New fancy fast computer with big screen and all that.

It's nice seeing leaves and grass on games. Who would have thought that. And racing games run. Really.

And the mechanical keyboard was a surprise. I see all the nerds nerding about it, but I never thought it would actually be as cool as this one is. And it was a cheap one. Or, at least, it was on sale. Looks a bit ugly with all the wrong RGB but who cares. Goes clac clac clac without being hard to type -- nothing like some sort of finger-eating typewriter, as I would have expected, for some reason. Maybe I just had the wrong types of mechanical keyboards in the past.

Took me half the time to render a video on Kdenlive, compared to the laptop.

The wifi card is not very fond of my wifi router, apparently, and internet speed drops every once in a while and I have to reset, which sucks. Haven't found a solution, but there are all sorts of useless pci warnings and errors that happen when I shut down the computer. Well, foo.

I did some dotfiles backup with `stow` and a syncthing'd all the things between the two computers.

It's nice having a fixed base and a screen that stays at eye-level. It's nice that `glmark2` gave my GPU a 5-digit score. Never seen that one before. Cintiq stays right next to me acting as a handy second screen when not in Krita-mode. It's all a bit less claustrophobic, one could say. This is good. 
