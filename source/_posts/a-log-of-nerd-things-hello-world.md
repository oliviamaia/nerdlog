---
title: 'a log of nerd things: hello world'
date: 2020-02-18 15:16:20
---

Hello, world!

Expect little; this blog is a learning space and a public wiki for my nerd findings in the world of UNIX and code in general. It's an attempt to register what I learn so I won't forget it the next day, or lose in in a sea of dotfiles. Also: how did you even end up here?

In any case, welcome. Leave your coat and shoes at the door and bring your laptop to the table.
