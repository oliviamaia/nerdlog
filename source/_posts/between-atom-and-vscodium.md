---
title: between atom and vscodium
date: 2021-03-14 16:44:50
---

To be fair, I would prefer being able to manage all my editing on the command line, because it does seem so much easier and more efficient. But turns out sometimes I need something that lets me see a tree of files and allows me some more visual management of things. Specially when I'm hopping from file to file, like when editing some configs (cof cof polybar). WELL. Atom, it seems is slowly being abandoned? Might be just me. I mean, they are both Microsoft now, anyway. Sadly, both are electron and quite elephantic, but in the end they also both work quite similarly. I started missing some features that Atom seemed to have and suddenly seem gone (missing extensions from the extension repository, very likely). So I'm back trying some VSCodium for a bit.

Maybe I could get used to the `micro` plugin that gives you a sidebar like one of those nerdy vim extensions also seem to do for you. I always end up giving up before I should when it comes to remembering keybindings.

Ramble ramble.

Anyway.

This is really mostly a post to write on VSCodium and test drive it a little.

Again.
